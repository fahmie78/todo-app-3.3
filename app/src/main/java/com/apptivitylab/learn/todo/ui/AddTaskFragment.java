package com.apptivitylab.learn.todo.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apptivitylab.learn.todo.R;

/**
 * Created by jkhong on 12/08/2016.
 */
public class AddTaskFragment extends Fragment {
    public static final String TAG = "AddTaskFragment";

    private AppCompatEditText mTaskEditText;
    private Button mAddButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_task, container, false);

        mAddButton = (Button) rootView.findViewById(R.id.fragment_addtask_btn_add);
        mTaskEditText = (AppCompatEditText) rootView.findViewById(R.id.fragment_addtask_et_task);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTask();
            }
        });
    }

    private void saveTask() {
        // TODO: This should save the newly entered item and display to a list
    }

}
