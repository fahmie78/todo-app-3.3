package com.apptivitylab.learn.todo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.apptivitylab.learn.todo.ui.AddTaskFragment;
import com.apptivitylab.learn.todo.ui.TodoListFragment;

/**
 * Created by jkhong on 12/08/2016.
 */
public class AddTaskActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        // Prepare ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.activity_add_task_title);

        // Set a AddTaskFragment as the default content fragment
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_add_task_vg_container, new AddTaskFragment())
                .commit();
    }
}
